package library;

public class  LibraryTest {
    public static void main(String[] args) {
        Library l = new Library();

        System.out.println("_____________________________________________________________________________________");

        //Adds 4 unique Content subclasses to the library
        l.addToLibrary(new Book("1984", 9780451524935l, "George Orwell"));
        l.addToLibrary(new DVD("Jaws", "Steven Spielberg"));
        l.addToLibrary(new Newspaper("NYT", 17));
        l.addToLibrary(new CD("Dark Side of the Moon", "Pink Floyd"));
        System.out.println();

        //Prints the information related to each piece of content and makes their ID's public to the user.
        l.displayContents();
        System.out.println();

        //Borrows 1984 from the library.
        l.borrowFromLibrary(0);
        //The 'available to borrow' for 1984 reads false.
        l.displayContents();
        System.out.println();

        //Tries to borrow NYT from the library but it is a Periodical, so there is an error msg.
        l.borrowFromLibrary(2);
        System.out.println();

        //Tries to remove 1984 from the library, but it cannot because it is currently borrowed. 1984 still appears in the listing.
        l.removeFromLibrary(0);
        l.displayContents();
        System.out.println();

        //Trying to borrow a book which is already borrowed, so there is an error msg.
        l.borrowFromLibrary(0);
        System.out.println();

        //Returns 1984 to the library, so 'available to borrow' will read true.
        l.returnToLibrary(0);
        l.displayContents();
        System.out.println();

        //Now that 1984 has beeen returned, it can now be removed from the library, it will not appear in the listing.
        l.removeFromLibrary(0);
        l.displayContents();
        System.out.println();

        //Trying to return a CD which has not been borrowed, so there is an error msg.
        l.returnToLibrary(3);
        System.out.println();

        //Trying to borrow content which does not exist, so there is an error msg.
        l.borrowFromLibrary(20);
        System.out.println("_____________________________________________________________________________________");
    }
}