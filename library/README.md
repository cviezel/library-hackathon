Class Hierarchy:

Content -> BorrowedContent, Periodical
BorrowedContent -> Book, CD, DVD
Periodical -> Newspaper

Library contains a HashMap<Integer, Content> containing all of the pieces of content in the library. When a new Content type is created,
it is assigned an ID, statically stored and incremented in the class, so all pieces of content in the library can be uniquely identified.

To interface with the library, users can run displayContents() to
get a listing of all pieces of content stored in the library.

Books, CDs, and DVDs can be borrowed, as they extend from the BorrowedContent superclass. Newspapers and any future classes added to Periodical cannot. A message will appear in syserr if a user tries to borrow or return a Periodical.

Periodicals cannot be borrowed, but they are still given an ID and will appear in the library listing.

If content has been borrowed, it cannot be removed from the library
until it has been returned.

Ex: 

id: 0, name: 1984, available to borrow: true, isbn: 9780451524935, author: George Orwell

id: 1, name: Jaws, available to borrow: true, director: Steven Spielberg

id: 2, name: NYT, issue: 17

id: 3, name: Dark Side of the Moon, available to borrow: true, artist: Pink Floyd

A user can then run the method borrowFromLibrary(1) to borrow Jaws, as its ID is 1.