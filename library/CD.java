package library;

public class CD extends BorrowedContent {
    private final String artist;

    public CD(String nameIn, String artistIn) {
        super(nameIn);
        artist = artistIn;
    }

    public String getArtist() {
        return artist;
    }

    @Override
    public String toString() {
        return super.toString() + ", artist: " + artist;
    }
}