package library;

public class DVD extends BorrowedContent {
    private final String director;

    public DVD(String nameIn, String directorIn) {
        super(nameIn);
        director = directorIn;
    }

    public String getDirector() {
        return director;
    }

    @Override
    public String toString() {
        return super.toString() + ", director: " + director;
    }
}