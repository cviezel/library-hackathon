package library;

public class Book extends BorrowedContent {
    private final long isbn;
    private final String author;

    public Book(String nameIn, long isbnIn, String authorIn) {
        super(nameIn);
        isbn = isbnIn;
        author = authorIn;
    }

    public long getIsbn() {
        return isbn;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return super.toString() + ", isbn: " + isbn + ", author: " + author;
    }
}