package library;

import java.util.HashMap;
import java.util.Map;

public class Library implements LibraryI {
    private final HashMap<Integer, Content> contents;

    public Library() {
        contents = new HashMap<>();
    }
    public void addToLibrary(Content c) {
        contents.put(c.getId(), c);
        System.out.println(c.getName() + " was added to the library.");
    }
    public void removeFromLibrary(int id) {
        if(contents.containsKey(id)) {
            if(contents.get(id) instanceof BorrowedContent) {
                if(!((BorrowedContent)contents.get(id)).getAvailable()) {
                    System.err.println("Cannot remove " + contents.get(id).getName() + " as it is currently rented out.");
                    return;
                }
            }
            else {
                System.err.println("Periodicals cannot be borrowed.");
            }
            System.out.println(contents.get(id).getName() + " was removed from the library.");
            contents.remove(id);
        }
        else {
            System.err.println("" + id + " is not a valid id");
        }
    }
    public void borrowFromLibrary(int id) {
        if(contents.containsKey(id)) {
            if(contents.get(id) instanceof BorrowedContent) {
                ((BorrowedContent)contents.get(id)).borrowFromLibrary();
            }
            else {
                System.err.println("Periodicals cannot be borrowed.");
            }
        }
        else {
            System.err.println("" + id + " is not a valid id");
        }
    }

    public void returnToLibrary(int id) {
        if(contents.containsKey(id)) {
            if(contents.get(id) instanceof BorrowedContent) {
                ((BorrowedContent)contents.get(id)).returnToLibray();
            }
            else {
                System.err.println("Periodicals cannot be borrowed.");
            }
        }
        else {
            System.err.println("" + id + " is not a valid id");
        }
    }

    public void displayContents() {
        for (Map.Entry<Integer, Content> set : contents.entrySet()) {
		    System.out.println(set.getValue());
		} 
    }
}