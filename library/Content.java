package library;

public abstract class Content {
    private String name;
    private static int idCount = 0;
    private final int id;
    public Content(String nameIn) {
        name = nameIn;
        id = idCount++;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameIn) {
        name = nameIn;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return "id: " + id + ", name: " + name;
    }
}