package library;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class BorrowedContent extends Content {
    private boolean available = true;

    public BorrowedContent(String name) {
        super(name);
    }
    public void borrowFromLibrary() {
        if(available) {
            available = false;
            //https://www.tutorialspoint.com/java/java_date_time.htm
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
            
            System.out.println(super.getName() + " was borrowed at " + ft.format(dNow) + ".");
        }
        else {
            System.err.println(super.getName() + " cannot be borrowed, as is taken.");
        }
    }
    public void returnToLibray() {
        if(available) {
            System.err.println(super.getName() + " cannot be returned, as it was never borrowed.");
        }
        else {
            available = true;
            //https://www.tutorialspoint.com/java/java_date_time.htm
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
            System.out.println(super.getName() + " was returned at " + ft.format(dNow) + ".");
        }
    }

    public boolean getAvailable() {
        return available;
    }

    @Override
    public String toString() {
        return super.toString() + ", available to borrow: " + available;
    }
}