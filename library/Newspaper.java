package library;

public class Newspaper extends Periodical {
    private final int issue;

    public Newspaper(String nameIn, int issueIn) {
        super(nameIn);
        issue = issueIn;
    }

    public long getIssue() {
        return issue;
    }

    @Override
    public String toString() {
        return super.toString() + ", issue: " + issue;
    }
}